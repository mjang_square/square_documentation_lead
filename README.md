# Mike Jang's Qualifications

Mike Jang's qualifications for the Square Documentation Lead position.

- Resume: mjangResume_square.docx
- Cover Letter: coverLetter_square.docx
- Writing Samples: listOfWritingSamples.md
- Speaking Experience: speakingExperience.md

